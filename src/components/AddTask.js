import { useState } from "react";

const AddTask = ({ onAdd }) => {
  const [text, setText] = useState("");
  const [date, setDate] = useState("");
  const [reminder, setReminder] = useState(false);
  const status = "todo";

  const onSubmit = (e) => {
    // submit event for the form
    e.preventDefault();

    if (!text) {
      alert("Please add task");
      // if user input empty test, pop alert window
      return;
    }

    onAdd({ text, date, reminder, status });

    setText("");
    setDate("");
    setReminder(false);
    // reset inpput fields
  };

  return (
    // addTask pannel is a togglable form pannel, users can create tasks from the panel
    <form className="add-form" onSubmit={onSubmit}>
      <div className="form-control">
        <label>Task</label>
        <input
          type="text"
          placeholder="Add Task"
          value={text}
          onChange={(e) => setText(e.target.value)}
        />
      </div>
      <div className="form-control">
        <label>Date</label>
        <input
          type="text"
          placeholder="Add Date"
          value={date}
          onChange={(e) => setDate(e.target.value)}
        />
      </div>
      <div className="form-control">
        <label>Reminder</label>
        <input
          type="checkbox"
          value={reminder}
          onChange={(e) => setReminder(e.currentTarget.checked)}
        />
      </div>

      <input className="btn" type="submit" value="Add task" />
    </form>
  );
};

export default AddTask;
