import Task from "./Task";

const Tasks = ({ tasks, onDelete, onToggle, onFinish }) => {
  return (
    <>
      {tasks.map((task) => (
        <Task
          key={task.id}
          task={task}
          onDelete={onDelete}
          onToggle={onToggle}
          onFinish={onFinish}
        />
      ))}
    </>
  );
};

export default Tasks;
