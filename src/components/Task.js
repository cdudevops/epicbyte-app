import { FaTimes, FaRegCheckCircle } from "react-icons/fa";

const Task = ({ task, onDelete, onToggle, onFinish }) => {
  return (
    <div
      className={`task ${task.reminder ? "reminder" : ""}`}
      // if task reminder = true the className will be "task reminder" and corresponding style will be applied
      onDoubleClick={() => onToggle(task.id)}
    >
      <h3>
        {task.text}
        <FaTimes
          style={{ color: "grey", cursor: "pointer" }}
          onClick={() => onDelete(task.id)}
        />
      </h3>
      <p>{task.date}</p>
      {task.status === "todo" ? (
        <FaRegCheckCircle onClick={() => onFinish(task.id)} />
      ) : (
        ""
      )}
    </div>
  );
};

export default Task;
