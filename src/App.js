import "./index.css";

// Import routing DOM
import React from "react";
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";

import Chat from "./pages/support";
import Todo from "./pages/todo";
import logo from "./img/logo.png";

class App extends React.Component {
  render() {

    // root component
    return (
        <div className="container">
          {/* routing for app */}
          <Router>
            <div>
              <nav className="nav">
                <a href="https://epicbyte-cdu.bitbucket.io/project/pages/index.html">
                  <img src={logo} alt="Epic Byte" className="logo"/>
                </a>
                <ul className="nav-btn">
                  <li>
                    <Link to="/support">Support</Link>
                  </li>
                  <li>
                    <Link to="/">ToDo</Link>
                  </li>
                </ul>
              </nav>

              {/* A <Switch> looks through its children <Route>s and
                renders the first one that matches the current URL. */}
              <Switch>
                <Route path="/support" exact component={Chat}>
                  <Support/>
                </Route>
                <Route path="/" component={Todo}>
                  <Home/>
                </Route>
              </Switch>
            </div>
            <footer>
              <h5>Epic Byte</h5>
              <p>Yuan, Beamish, Wozniak, Hollingworth </p>
              <p>
                CDU HIT226 | &#169; 2021 |{" "}
                <a href="https://epicbyte-cdu.bitbucket.io/project/pages/index.html">
                  Home page
                </a>
              </p>
            </footer>
          </Router>
        </div>
    );
  }
}

export default App;

// routing
function Home() {
  return Todo();
}

function Support() {
  return Chat();
}
