import React from 'react';
import {useForm} from "react-hook-form";

import "./support.css";


export default function Chat() {
    // Deafult tech support responses
    const techSupport = [
        'Have you tried restartng your browser?', 
        'How can I help you?',
        'Yes of course we can do that for you.',
        'Unfortunately we cannot support that on our platform right now.',
        'Yes, our premium plan is around AU$15.99 per month depending on selected features',
        'From the home page, click on "Contact" and send out staff a message. They will contact you to expore further.',
        'Yes',
        'No, sorry',
        'Absolutely, just ask',
        'Yes, that happens sometimes',
        'You may need to apply updates to your device to get that fetaure working',
        'Please call our office, we are open 9:00 am - 5:00pm'
    ];

        // Use react-hook-form to handle the form
    const {register, handleSubmit} = useForm();

    const onSubmit = (data) =>{
        // get the input field
        var responseInput = document.getElementById("respond");

        // Check if there is a question before responding
        if ( !data['response']) {
            alert("Please ask a question before sending.");
            responseInput.focus(); 
            return;
          }

        // Create a new p tag for the valid question/input
        var newElement = document.createElement("p");
        newElement.className = "response chat"
        var node = document.createTextNode(data['response']);
        newElement.appendChild(node);
        // Seelect the conversaton div to add question to
        var element = document.getElementById("chatconvo");
        // Add response to chat dialogue
        element.appendChild(newElement);

        // Add Tech Support Response
        const random = Math.floor(Math.random() * techSupport.length);
        // Select a random reponse
        var  botresponse = (random, techSupport[random]);
        // Create the HTML element for the p tag response
        var para = document.createElement("p");
        para.className = "bot chat"
        var botnode = document.createTextNode(botresponse);
        para.appendChild(botnode);
        // Select the div where the response will be added
        var botelement = document.getElementById("chatconvo");
        // Add the response
        botelement.appendChild(para);

        // Clear input field
        responseInput.value = "";   
        responseInput.focus();     
    }

    return (
        <div className="support">
            <h1>Tech Support</h1>
            <p>Send a message and our friendly staff will respond.</p>
            <div className="chatconvo" id="chatconvo">
                {/* Add tech support log / conversation here */}
            </div>
            <form onSubmit= {handleSubmit(onSubmit)} >
                <input id = "respond" type="text" placeholder="Type your question..." {...register('response')} className="question"/>
                <input type="submit" value = "Send" className="sendButton"/>
            </form>
        </div>
    )
};




  

